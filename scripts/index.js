
var NROW = 10;
var NCOL = 20;
var PLAYERS = ["Derek", "Mom", "Dad", "TJ", "Emily", "Nathan", "Bethany"];
//var PLAYERS = ["Derek", "Mom", "Dad", "TJ"];
var ALPHA = .4;

var grid_div_id = "battleship_grid_div";
//var player_select_id = "player_select";
var player_table_id = "player_table_div";
var next_turn_button_id = "next_turn_button";
var player_radio_name = "player_radio";
var shot_number_name = "shot_number";
var erase_val = "__erase__";

var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
//================================================================
//Set up player dropdown
//================================================================

// var select = document.getElementById(player_select_id);
// for(var i = 0; i < PLAYERS.length; i++){
//     var opt = document.createElement("option");
//     opt.value = i;
//     opt.text = PLAYERS[i];
//     select.add(opt, null);
// }

//================================================================
//Set up color palette
//================================================================
// let colorMap = new jPalette.ColorMap(100, [
//     new jPalette.Color(255, 0, 0, 255),
//     new jPalette.Color(0, 255, 0, 255),
//     new jPalette.Color(0, 0, 255, 255),
//   ]);
var colorMap = new ColorMap(1000, [
    new Color(255,   0,   0, ALPHA),
    new Color(255, 128,   0, ALPHA),
    new Color(255, 255,   0, ALPHA),
    new Color(0  , 255,   0, ALPHA),
    new Color(0  , 255,  128, ALPHA),
    new Color(0  , 255,  255, ALPHA),
    new Color(0  ,   0,  255, ALPHA),
    new Color(128,   0,  255, ALPHA),
    new Color(255,   0,  255, ALPHA)
  ]);


function colorToRGBA(color, alpha){
    return "rgba(" + color.r + "," + color.g + "," + color.b + "," + alpha + ")";
}

//var colorMap = jPalette.ColorMap.get('rainbow')(1000);

var colorStep = 1/PLAYERS.length;
//var playerColors = new Array(PLAYERS.length);
var playerMap = new Map();
for(var i = 0; i < PLAYERS.length; i++){
    //playerMap.set(PLAYERS[i], colorToRGBA(colorMap.getColor(i*colorStep), ALPHA));
    playerMap.set(i, colorToRGBA(colorMap.getColor(i*colorStep), ALPHA));
}

// Normalized index between 0 and 1
//let color = colorMap.getColor(0.3);

//console.log(color);
// Color {r: 105, g: 149, b: 0, a: 255}
//console.log(color.rgb());

//================================================================
//Set up player table
//================================================================
var player_table = document.createElement("table");
for(var i = 0; i < PLAYERS.length + 1; i++){
    var tr = player_table.appendChild(document.createElement('tr'));

    var radioCell = tr.appendChild(document.createElement("td"));
    var nameCell = tr.appendChild(document.createElement("td"));

    var radioButton = document.createElement("input");
    radioButton.type="radio";
    radioButton.name=player_radio_name;
    if(i == 0){
        radioButton.checked=true;
    }

    if(i < PLAYERS.length){
        //radioCell.style.backgroundColor = playerMap.get(PLAYERS[i]);
        radioCell.style.backgroundColor = playerMap.get(i);
        //radioButton.value = PLAYERS[i];
        radioButton.value = i;
        //nameCell.style.backgroundColor = playerMap.get(PLAYERS[i]);
        nameCell.style.backgroundColor = playerMap.get(i);
        nameCell.innerHTML = PLAYERS[i];
    } else {
        radioButton.name=player_radio_name;
        radioButton.value = erase_val;
        nameCell.innerHTML = "ERASE";
    }
    radioCell.appendChild(radioButton);   
}

var table_div = document.getElementById(player_table_id);
table_div.appendChild(player_table);

//================================================================
//Set up clickable grid
//================================================================
//Code modified from:
//https://stackoverflow.com/questions/9140101/creating-a-clickable-grid-in-a-web-browser
function clickableGrid( rows, cols, callback ){
    var i=0;
    var grid = document.createElement('table');
    grid.className = 'grid';
    for (var r=0;r<rows;++r){
        var tr = grid.appendChild(document.createElement('tr'));
        for (var c=0;c<cols;++c){
            var cell = tr.appendChild(document.createElement('td'));
            if(c == 0 && r >= 1){
                //cell.innerHTML =  r;
                cell.innerHTML =  alphabet.charAt((r % alphabet.length)-1);
            }
            if(r == 0 && c >= 1){
                cell.innerHTML = c;
            }
            if(r != 0 && c != 0){
                cell.addEventListener('click',(function(el,r,c,i){
                    return function(){
                        callback(el,r,c,i);
                    }
                })(cell,r,c,i),false);
            } else {
                cell.style.backgroundColor = "#EEEEEE";
            }
        }
    }
    return grid;
}


var grid = clickableGrid(NROW+1,NCOL+1,function(el,row,col,i){


    var selectedPlayer = $('input[name="' + player_radio_name + '"]:checked').val();
    var turnNumber = $('input[name="' + shot_number_name + '"]').val();
    if(selectedPlayer == erase_val){
        el.style.backgroundColor = "";
        el.innerHTML = "";
    } else {
        el.style.backgroundColor = playerMap.get(parseInt(selectedPlayer));
        el.innerHTML = turnNumber;
    }

    console.log("You clicked on element:",el);
    console.log("You clicked on row:",row);
    console.log("You clicked on col:",col);
    console.log("You clicked on item #:",i);
    console.log("selected value:",selectedPlayer );
    console.log("selected player:",selectedPlayer);
    console.log("player color:",playerMap.get(selectedPlayer));
    console.log("turn number:",turnNumber);
});

var gridDiv = document.getElementById(grid_div_id);
gridDiv.appendChild(grid);
//document.body.appendChild(grid);

function incrementTurn(){
    var selectedPlayer = $('input[name="' + player_radio_name + '"]:checked');
    var turnInput =$('input[name="' + shot_number_name + '"]');
    var nextPlayer = (parseInt(selectedPlayer.val()) + 1) % PLAYERS.length;
    console.log(selectedPlayer.val());
    console.log(nextPlayer);
    $('input[name="' + player_radio_name + '"][value="' + nextPlayer + '"]').prop("checked", true);
    turnInput.val(parseInt(turnInput.val()) + 1);
    //selectedPlayer.prop("checked", true);
    
}

document.getElementById(next_turn_button_id).addEventListener('click',incrementTurn);